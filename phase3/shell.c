#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pwd.h>
#include <sys/stat.h>
#include <fcntl.h>

const char *getUserName()
{
	uid_t uid = geteuid();
	struct passwd *pw = getpwuid(uid);
	if (pw)
	{
		return pw->pw_name;
	}
	return "";
}

int main(int argc, char* argv[]){
	int length;
	int arg_counter;

	char* host_name = (char*)malloc(100);
	char* pwd = (char*)malloc(1000);
	char* command_line = (char *) malloc(20);

	size_t command_size = 300;
	argv = (char **) malloc(100 * sizeof(char *));

	while(1){
		gethostname(host_name, 100);
		getcwd(pwd, 1000);

		fprintf(stdout, "%s@%s %s $ ", getUserName(), host_name, pwd);

		length = getline(&command_line, &command_size, stdin);
		command_line[length-1] = '\0';
		arg_counter = 0;

		for(argv[arg_counter] = strtok(command_line, " ");
			argv[arg_counter] != NULL;
			arg_counter++, argv[arg_counter] = strtok(NULL, " "));


		char* output_file = NULL;

		int i;
		for(i = 0; i < arg_counter; i++){
			if(strcmp(argv[i], ">") == 0){
				argv[i] = NULL;
				if(argv[i + 1] != NULL){
					output_file = argv[i+1];
				}
			}
		}


		if(strcmp(argv[0], "cd") == 0) {
			chdir(argv[1]);
			continue;
		}

		pid_t p = fork();

		if (p == 0){
			if(output_file)
			{
				int fd = open(output_file, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
				dup2(fd, 1);
				dup2(fd, 2);
				close(fd);
			}
			execvp(argv[0], argv);
			abort();
		}
		else {
			dup2(1,1);
			dup2(2,2);
			wait(NULL);
		}

		if(strcmp(command_line, "exit") == 0){
			break;
		}
	}

	free(argv);
	free(host_name);
	free(pwd);
	free(command_line);

	return 0;
}
