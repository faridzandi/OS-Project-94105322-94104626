#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/kthread.h>
#include <linux/delay.h>

// containing the newly created kernel thread. will be started and finished,
// when the module is inserted and removed.
struct task_struct *thread;

// containing the last known memory allocation for each pid.
unsigned long int prev[70000];

// indicating when the thread should stop
int stop;

// a thread comparing the memory allocation of processes every second and
// logs every change.
int thread_function(void *data){

	// a flag set to zero in the beginning so that in the first time the changes are
	// not shown. will be set to one after the first loop iteration.
	int flag = 0;

	int i;
	for(i = 0 ; i < 70000; i++) prev[i] = 0;

	while(!stop){
		struct task_struct* task;

		// a macro, helping the iterate over all of the processes in the system.
		for_each_process(task){
			// tasks might not have mm.
			if(task->mm){
				if(task->mm->total_vm != prev[task->pid]){
					if(flag){
						long int diff = task->mm->total_vm - prev[task->pid];

						if(diff > 0){
							printk(KERN_INFO "-----MEMORY_PAGES---- %lu is allocated for %d\n", diff, task->pid);
						}else{
							printk(KERN_INFO "-----MEMORY_PAGES---- %lu is freed by %d\n", (-1) * diff, task->pid);
						}
					}
					// saving the current state for comparing to new values in the next run.
					prev[task->pid] = task->mm->total_vm;
				}
			}
		}
		// waiting one second before calculating the differences.
		ssleep(1);

		flag = 1;
	}
	return 0;
}

int init_module(void)
{
	printk(KERN_INFO "-----MEMORY_PAGES----- Started Tracking Changes In Memory Management");
	stop = 0;
	thread = kthread_create(&thread_function,NULL,"MEMORY_PAGES");
	thread = kthread_run(&thread_function,NULL,"MEMORY_PAGES");
	return 0;
}

void cleanup_module(void)
{
	stop = 1;
	printk(KERN_INFO "-----MEMORY_PAGES----- Ended Tracking Changes In Memory Management");
	kthread_stop(thread);
}
