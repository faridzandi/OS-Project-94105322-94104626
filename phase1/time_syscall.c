#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>


void print_time();

int main()
{
	print_time();
	return 0;
}

// will print the current time in the format: 
// Day_of_Week Month Day_of_Month Hour Minute Second
 
void print_time(){
	printf("using system call 201 to get current time\n");
	
	// adding this instruction caused the system not to return -1 status code.	
	system("");	

	// system call 201 will return the number of seconds passed since January First 1970.
	time_t current_time = syscall(201); 
	
	
	if(current_time == -1){
		printf("An error occured while getting time using the system call.\n");
		printf("Using time function to get current time");

		// if system call couldn't answer the time function can be used instead.
		current_time = time(0);	
		char* current_time_string = ctime(&current_time); 
	        printf("current time is %s\n", current_time_string);
	} else {
		// translating the timestamp to human readable format.
		char* current_time_string = ctime(&current_time);
		printf("current time is %s\n.", current_time_string);
	}
}
	

