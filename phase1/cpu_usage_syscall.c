#include<stdio.h>

void print_cpu_usage();
 
typedef struct cpu_stat_struct{
	long double user; 
	long double nice; 
	long double system;
	long double idle; 
	long double iowait; 
	long double irq; 
	long double softirq; 
	long double steal; 
	long double guest; 
	long double guest_nice;
} cpu_stat;

int main()
{
	print_cpu_usage(); 
	return 0; 
}

// will print the total cpu usage calculated in the time interval starting now and ending one second later.

void print_cpu_usage(){
	// making two cpu_stat instances to save the data parsed from the files.
	// stat1 contains the stat for "now".
	// stat2 contains the stat for one second later. 
	
	cpu_stat stat1;
	cpu_stat stat2; 	
	FILE* f; 
	
	// reading the the first line in file /proc/stat which contains ticks for all of the cores of the cpu. 
	// the format for this file is as follows: 
	// cpu_name user_ticks nice_ticks system_ticks idle_ticks iowait_ticks irq_ticks softirq_ticks steal_ticks guest_ticks guest_nice_ticks
		
	f = fopen("/proc/stat", "r");
		
	fscanf(f, 
	    "%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf",
	    &(stat1.user), 
	    &(stat1.nice), 
	    &(stat1.system), 
	    &(stat1.idle), 
	    &(stat1.iowait), 
	    &(stat1.irq), 
	    &(stat1.softirq), 
	    &(stat1.steal), 
	    &(stat1.guest), 
	    &(stat1.guest_nice)
	);
	fclose(f);
	
	// wait 1 second and then read the /proc/stat file again.
 
	sleep(1);
	f = fopen("/proc/stat", "r"); 
	fscanf(f,	
	    "%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf",
    	    &(stat2.user), 
	    &(stat2.nice), 
	    &(stat2.system), 
	    &(stat2.idle), 
	    &(stat2.iowait), 
	    &(stat2.irq), 
	    &(stat2.softirq), 
	    &(stat2.steal), 
	    &(stat2.guest), 
	    &(stat2.guest_nice)
	);
	
	// each tick is either used or idle.
	// used_ticks1 is number of used ticks from boot to now. 
	// used_ticks2 is number of used ticks from boot to one second later. 
	// idle_ticks1 is number of idle ticks from boot to now. 
	// idle_ticks2 is number of idle ticks from boot to one second later. 

	
	long double used_ticks1 = stat1.user + stat1.nice + stat1.system;
	
	long double used_ticks2 = stat2.user + stat2.nice + stat2.system;

	long double idle_ticks1 = stat1.idle + stat1.iowait + stat1.irq + stat1.softirq + stat1.steal + stat1.guest + stat1.guest_nice; 

	long double idle_ticks2 = stat2.idle + stat2.iowait + stat2.irq + stat2.softirq + stat2.steal + stat2.guest + stat2.guest_nice;

	long double total_ticks1 = used_ticks1 + idle_ticks1;

	long double total_ticks2 = used_ticks2 + idle_ticks2;
	
	// cpu_usage is ratio of used ticks in this interval to number of all of the ticks. 

	long double cpu_usage = (used_ticks2 - used_ticks1) / (total_ticks2 - total_ticks1);
	
	cpu_usage *= 100;

	printf("current cpu usage percentage :%Lf\n" , cpu_usage);	
}
